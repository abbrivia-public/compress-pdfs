# PDFs komprimieren für DATEV

DATEV bietet die Möglichkeit Rechnungen  online zu speichern bzw. zu erfassen. 
Wie kann man die PDF Dateien so klein wie möglich bekommen, so das sie den Platz
nicht zu schnell aufbrauchen? Hier ist eine Lösung mit dem Ghostscript für 
Linux/Unix etc.

Eine relevante [Diskussion](https://forum.jtl-software.de/threads/pdf-dateigroesse-bei-rechnungen-kleiner-bekommen.95463/) dazu.

Und weitere hilfsreiche Links:

https://www.ferd-net.de/upload/Rechtliche-Rahmenbedingungen-eRechnung.pdf

https://www.datev.de/berechnungstool-belegverwaltung-online/berechnungstool.html

https://docupub.com/pdfcompress/


One picture is worth thousand words they say. What about its storage requirements? A digital image file can take disproportionally more disk space as to its informational content. Text documents stored as image files are the best/worst examples here. Paper bills that are printed out and later scanned into PDFs can take hundreds of kBytes of space. While the original text or xlsx document weights only a few kB we thus have a lot of overhead. 

Often the mere time spent in processing these documents costs more than their storage space. So who cares? Well, in some application the size (of file) matters. Such is the case with the bookkeeping system Datev popular in Germany. Companies that upload large PDF documents (bills, invoices, etc.) have to pay extra for their hosting and storage. So it pays back to keep your documents only as large as necessary. The trade-off is the image quality. On the other hand, once the bill is digitized and entered into the bookkeeping database no one is looking at the original. It is only there just in case (an audit could be such possible use case). So we can sacrifice the picture quality quite a bit.

Alas, to not to waste the storage space we have developed a script. It takes all the PDFs and shrinks them in size and resolution. Just so they are just passable for the practical purposes. 

The main advantage of a scripted solution here is that it does not require manual intervention. You do not need to open and process each document one by one. You don't even need an expensive Adobe software installed. In fact the GhostScript software used here is completely free of charge. What's not to like? 

# How To Install

install Ghostscript `sudo apt install ghostscript`

Open ~/.bashrc file with any editor and paste the function code there.

Run `source ~/.bashrc` for bash to re-read that config file (needed only on install)
