#!/bin/bash
# (c) Abbrivia GmbH 2019
# make backups of your data! 
# there is no warranty for this work 
# see the license https://gitlab.com/abbrivia-public/compress-pdfs/blob/master/LICENSE
# if you accept the license run this in the directory where you have the files you want to convert.
# GhostScript should be installed for this to work.
# the compressed files will be stored in the ./compressed directory as originalname-compressed.pdf
# 
function compress() {
        echo $(pwd);
        mkdir -p ./compressed
        find . -type f -name "*.pdf" -not -name "*compressed*" -exec sh -c 'if [ ! -f ./compressed/"${1%.pdf}-compressed.pdf" ]; then
echo "compressing $1"; 
/usr/bin/gs -q -dNOPAUSE -dBATCH -dPDFA=1 \
-dDownsampleColorImages=true \
-dCompatibilityLevel=1.4 \
-dPDFSETTINGS=/screen \
-dEmbedAllFonts=true \
-dSubsetFonts=true \
-dAutoRotatePages=/None \
-dColorImageDownsampleType=/Bicubic \
-dColorImageResolution=96 \
-dGrayImageDownsampleType=/Bicubic \
-dGrayImageResolution=96 \
-dMonoImageDownsampleType=/Subsample \
-dMonoImageResolution=96 \
-sDEVICE=pdfwrite -sOutputFile=./compressed/"${1%.pdf}-compressed.pdf" "$1" 
fi ' sh {} \; 2>/dev/null;
}
# run export to use it right away, otherwide add it to your ~/.bashrc
#export compress